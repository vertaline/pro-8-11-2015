'use strict';

function genericOnClick(text) {
  chrome.tabs.query({
    'active': true,
    'currentWindow': true
  }, function(tabs) {
    chrome.tabs.sendMessage(tabs[0].id, {
      'textToLocate': text
    });
  });
}
// Create context menu item
chrome.contextMenus.create({
  type: 'normal', // the default textual option
  title: 'Show on Map', // the text that is displayed
  contexts: ['selection'], // only display on selected text
  onclick: function(obj) {
    genericOnClick(obj.selectionText);
  },
  documentUrlPatterns: ['<all_urls>'] // show on all addresses
});

chrome.browserAction.onClicked.addListener(function(tab) {
  genericOnClick('');
});
