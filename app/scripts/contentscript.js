'use strict';

var includeScirpt = function(source) {
  var script = document.createElement('script');
  script.src = source;
  (document.head||document.documentElement).appendChild(script);
  script.onload = function() {
    script.parentNode.removeChild(script);
  };
};

includeScirpt(chrome.extension.getURL('scripts/script.js'));

document.addEventListener('P44appLoaded', function() {
  var willLoadGoogleMaps = false;
  for (var i = 0; i < document.scripts.length; i++) { if ((document.scripts[i].src.indexOf("maps.googleapis.com") > -1)||((document.scripts[i].src.indexOf("maps.gstatic.com") > -1))){
    willLoadGoogleMaps = true;
  }}
  if (!willLoadGoogleMaps) includeScirpt('https://maps.googleapis.com/maps/api/js?v=3.exp&libraries=places&sensor=false&callback=initP44App');
  else window.initP44Warning();
});

chrome.extension.onMessage.addListener(function (message, sender, callback) {
  var actualCode;
  if (message.textToLocate.length > 0) {
    actualCode = '(' + function(text) {
      if(window.P44) window.P44.locate(text);
      else window.P44Warning();
    } + ')(' + JSON.stringify(message.textToLocate) + ');';
  } else {
    actualCode = '(' + function() {
      if (window.P44) window.P44.toggleMap();
      else window.P44Warning();
    } + ')()';
  }
  var script = document.createElement('script');
  script.textContent = actualCode;
  (document.head||document.documentElement).appendChild(script);
  script.parentNode.removeChild(script);
});
