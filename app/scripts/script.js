window.initP44App = function() {
  window.P44 = {
    key: 'AIzaSyB74_eZY4tRhu_iANv6a39L9aIiZE2Xlio',
    keyPlaces: 'AIzaSyCzm5qsHlsUAINf5zUlBF4OKLhAXd4NTS4',

    bounds: new google.maps.LatLngBounds(),
    autocompleteService: new google.maps.places.AutocompleteService(),
    places: [],

    // search start
    locate: function(text) {
      this.getMap();
      this.searchText = text;
      this.input.value = text;
      this.setPredictions(text);
    },

    setPredictions: function(search) {
      this.autocompleteService.getPlacePredictions({ input: search }, this.setSuggestions);
    },

    getPredictions: function(search) {
      this.autocompleteService.getPlacePredictions({ input: search }, this.displaySuggestions);
    },

    setSuggestions: function(predictions, status) {
      if (!predictions || predictions.length === 0) {
        var newSearch = window.P44.searchText.substring(0, window.P44.searchText.length - 1);
        if (newSearch.length > 1) {
          window.P44.searchText = newSearch;
          return window.P44.setPredictions(newSearch);
        } else return false;
      }
      window.P44.sortPredictions(predictions);
      window.P44.showOnMap(predictions[0].place_id);
      window.P44.displaySuggestions(predictions, status, 0);
    },

    displaySuggestions: function(predictions, status, activeIndex) {
      var currentIndex = -1;
      if (window.P44.currentPlace) {
        var currentPrediction = window.P44.getPredictionById(predictions, window.P44.currentPlace.place_id);
        currentIndex = predictions.indexOf(currentPrediction);
      }
      var index = activeIndex === 0 ? activeIndex : currentIndex;
      window.P44.sortPredictions(predictions);
      var suggestions = document.getElementById('p44-suggestions');
      suggestions.innerHTML = '';
      if (!predictions || predictions.length === 0) return false;
      predictions.forEach(function(prediction, i) {
        var li = document.createElement('li');
        li.className = "button-common-p44";
        li.textContent = prediction.description;
        li.dataset.place = prediction.place_id;
        if (i === index) li.className = li.className + ' active-p44';
        suggestions.appendChild(li);
      });
      var liList = suggestions.children;
      Array.prototype.slice.call(liList,0).forEach(function(li) {
        li.addEventListener('click', function(evt) {
          var active = document.querySelector('#p44-suggestions li.active-p44');
          if (active) active.className = active.className.replace('active-p44', '');
          evt.target.className = evt.target.className + ' active-p44'
          if (window.P44.currentPlace) window.P44.editPlace(this.dataset.place);
          else window.P44.showOnMap(this.dataset.place);
        });
      });
    },

    findSuggestions: function(evt) {
      var search = evt.target.value;
      if (search.length > 0) {
        window.P44.searchText = search;
        window.P44.getPredictions(search);
      }
    },
    // search end

    // markers start
    showOnMap: function(placeId) {
      var _this = this;
      this.getPlaceService().getDetails({
        placeId: placeId
      }, function(place, status) {
        if (status === google.maps.places.PlacesServiceStatus.OK) {
          _this.setNewPLace(place);
        }
      });
    },

    setNewPLace: function(place) {
      if (!place) return false;
      var samePlace = this.getPlaceById(place.place_id);
      if (samePlace) {
        this.currentPlace = samePlace;
        this.removePlace(samePlace);
      }
      place.searchText = this.searchText;
      this.currentPlace = place;
      this.fitBound(place.geometry);
      this.places.push(place);
      this.addMarker(place);
      this.updateNav();
    },

    addMarker: function(place, isFixed) {
      var _this = this;
      this.closeAllInfoWindows();
      this.popup.style.display = 'initial';
      var marker = new google.maps.Marker({
        map: _this.getMap(),
        animation: google.maps.Animation.DROP,
        position: place.geometry.location,
        title: place.name,
        anchorPoint: new google.maps.Point(0, -32)
      });
      marker.setIcon({
        url: place.icon,
        size: new google.maps.Size(71, 71),
        origin: new google.maps.Point(0, 0),
        anchor: new google.maps.Point(17, 34),
        scaledSize: new google.maps.Size(35, 35)
      });
      marker.addListener('click', function() {
        window.P44.setCurrentPlace(place);
      });
      place.marker = marker;
      this.showInfo(place);
    },

    removeMarker: function(evt) {
      var placeId = evt.target.dataset.place;
      var place = window.P44.getPlaceById(placeId);
      window.P44.removePlace(place);
    },
    // markers end

    // info windows start
    showInfo: function(place) {
      var infoWindow = new google.maps.InfoWindow();
      var marker = place.marker;
      var content = this.infoContent(place);
      infoWindow.setContent(content.innerHTML);
      infoWindow.open(this.getMap(), marker);
      marker.addListener('click', function() {
        window.P44.closeAllInfoWindows();
        infoWindow.open(this.getMap(), marker);
      });
      if (!infoWindow.binded) {
        google.maps.event.addListener(infoWindow, 'domready', function() {
          document.querySelector('.p44-remove-button').addEventListener('click', window.P44.removeMarker);
          infoWindow.binded = true;
        });
      }
      place.infoWindow = infoWindow;
    },

    closeAllInfoWindows: function() {
      this.places.forEach(function(place) {
        var infoWindow = place.infoWindow;
        if (infoWindow) infoWindow.close();
      });
    },

    infoContent: function(place) {
      var address = this.getAddress(place);
      var photo = this.getPhoto(place);
      var content = document.createElement('div');
      var body = document.createElement('div');
      var deleteDiv = document.createElement('div');
      if (photo) {
        var img = document.createElement('img');
        img.src = photo;
        content.appendChild(img);
      }
      body.innerHTML = '<strong>' + place.name + '</strong><br>' + address;
      content.appendChild(body);
      deleteDiv.innerHTML = '<button class="p44-remove-button button-common-p44 active-p44" data-place="' + place.place_id + '">Remove</button>';
      deleteDiv.style.borderTopWidth = '1px';
      deleteDiv.style.borderTopStyle = 'solid';
      deleteDiv.style.borderTopColor = 'rgb(204, 204, 204)';
      deleteDiv.style.marginTop = '9px';
      deleteDiv.style.paddingTop = '6px';
      deleteDiv.style.width = '100%';
      content.appendChild(deleteDiv);
      return content;
    },

    getAddress: function(place) {
      var address = '';
      if (place.address_components) {
        address = [
          (place.address_components[0] && place.address_components[0].short_name || ''),
          (place.address_components[1] && place.address_components[1].short_name || ''),
          (place.address_components[2] && place.address_components[2].short_name || '')
        ].join(' ');
      }
      return address;
    },

    getPhoto: function(place) {
      if (!place.photos) return false;
      return place.photos[0].getUrl({maxWidth: 200, maxHeight: 125});
    },
    // info windows end

    // fit maps start
    fitMarkers: function() {
      this.closeAllInfoWindows();
      if (this.places.length == 1) {
        this.fitBound(this.places[0].geometry);
      } else if (this.places.length > 1) {
        var bounds = new google.maps.LatLngBounds();
        this.places.forEach(function(place) {
          bounds.extend(place.marker.getPosition());
        });
        this.getMap().fitBounds(bounds);
      }
    },

    fitBound: function(geometry) {
      var setZoom = false;
      if (!geometry.viewport) {
        this.bounds = new google.maps.LatLngBounds().extend(geometry.location);
        setZoom = true;
      } else this.bounds = new google.maps.LatLngBounds().union(geometry.viewport);
      this.getMap().fitBounds(this.bounds);
      if (setZoom) this.map.setZoom(9);
    },
    // fit maps end

    // helpers start
    getMap: function() {
      if (!this.map) {
        this.initContainer();
        var mapOptions = {
          zoom: 1,
          center: {lat: 0, lng: 0}
        };
        this.map = new google.maps.Map(document.getElementById('map-p44'), mapOptions);
        this.isPanoramaVisible = false;
        var panorama = new google.maps.StreetViewPanorama(document.getElementById('map-p44'),
          { pov: { heading: 34, pitch: 10 } });
        panorama.setVisible(this.isPanoramaVisible);
        this.map.setStreetView(panorama);
      }
      return this.map;
    },

    toggleMap: function() {
      this.getMap();
      var display = this.popup.style.display;
      if (display === 'none' || display === '') {
        this.popup.style.display = 'initial';
        google.maps.event.trigger(this.getMap(), 'resize');
      } else {
        this.popup.style.display = 'none';
      }
    },

    getPlaceService: function() {
      if (!this.placeService) {
        this.placeService = new google.maps.places.PlacesService(this.getMap());
      }
      return this.placeService;
    },

    removePlace: function(place) {
      if (!place) return false;
      place.marker.setMap(null);
      var index = this.places.indexOf(place);
      this.places.splice(index, 1);
      this.currentPlace = null;
    },

    editPlace: function(placeId) {
      this.getPlaceService().getDetails({
        placeId: placeId
      }, function(place, status) {
        if (status === google.maps.places.PlacesServiceStatus.OK) {
          window.P44.updatePlace(place);
        }
      });
    },

    updatePlace: function(place) {
      var oldPlace = this.currentPlace;
      if (oldPlace.marker) oldPlace.marker.setMap(null);
      var index = this.places.indexOf(oldPlace);
      this.places[index] = place;
      place.searchText = this.searchText;
      this.currentPlace = place;
      this.fitBound(place.geometry);
      this.addMarker(place);
    },

    setCurrentPlace: function(place) {
      if (!place) return false;
      this.currentPlace = place;
      this.getPredictions(place.searchText);
      this.input.value = place.searchText;
      this.searchText = place.searchText;
      this.closeAllInfoWindows();
      this.updateNav();
      place.infoWindow.open(this.getMap(), place.marker);
    },

    setCurrentPlaceEvt: function(evt) {
      var place = this.getPlaceById(evt.target.dataset.place);
      this.setCurrentPlace(place);
      this.fitBound(this.currentPlace.geometry);
    },

    getPlaceById: function(placeId) {
      var place;
      this.places.forEach(function(p) {
        if (p.place_id == placeId) {
          place = p;
          return;
        }
      });
      return place;
    },

    getPredictionById: function(predictions, placeId) {
      var prediction;
      predictions.forEach(function(p) {
        if (p.place_id == placeId) {
          prediction = p;
          return;
        }
      });
      return prediction;
    },

    sortPredictions: function(predictions) {
      if (predictions) return false;
      var priorites = [];
      var compare = function (a,b) {
        if (a.prioritet === b.prioritet) {
          return (a.description.length < b.description) ? -1 : 1;
        } else {
          return (a.prioritet > b.prioritet) ? -1 : 1;
        }
        return 0;
      };
      predictions.forEach(function(pr) {
        var prioritet = 0;
        if (pr.types.indexOf('country') !== -1) prioritet += 5;
        if (pr.types.indexOf('locality') !== -1) prioritet++;
        if (pr.types.indexOf('political') !== -1) prioritet++;
        if (pr.types.indexOf('geocode') !== -1) prioritet++;
        pr.prioritet = prioritet;
      });
      return predictions.sort(compare);
    },

    updateNav: function() {
      var prevEl = document.getElementById('triangle-left-p44');
      var nextEl = document.getElementById('triangle-right-p44');
      if (this.currentPlace) {
        var prevPlace = this.previousPlace();
        var nextPlace = this.nextPlace();
      } else var prevPlace = this.places[this.places.length - 1];
      prevEl.className = prevEl.className.replace('visible-p44', '');
      nextEl.className = nextEl.className.replace('visible-p44', '');
      if (prevPlace) {
        prevEl.className = prevEl.className + ' visible-p44';
        prevEl.dataset.place = prevPlace.place_id;
      }
      if (nextPlace) {
        nextEl.className = nextEl.className + ' visible-p44';
        nextEl.dataset.place = nextPlace.place_id;
      }
    },

    nextPlace: function() {
      var placesCount = this.places.length;
      if (placesCount < 2) return false;
      var index = this.places.indexOf(this.currentPlace);
      if (placesCount - 1 == index || index == -1) return false;
      return this.places[index + 1];
    },

    previousPlace: function() {
      var placesCount = this.places.length;
      if (placesCount < 2) return false;
      var index = this.places.indexOf(this.currentPlace);
      if (index === 0 || index == -1) return false;
      return this.places[index - 1];
    },

    newPlace: function() {
      this.currentPlace = null;
      this.closeAllInfoWindows();
      this.updateNav();
      this.input.value = '';
      document.getElementById('p44-suggestions').innerHTML = '';
    },
    // helpers end

    // init functions start
    initContainer: function() {
      var body = document.getElementsByTagName('body')[0];
      var popup = document.createElement('div');
      this.popup = popup;
      popup.className = 'overlay-p44';
      popup.id = 'popup-p44';
      popup.innerHTML = '<div class="popup-p44">' +
        '<div class="head-p44">'+
          '<div class="close-p44">x</div>' +
        '</div>'+
        '<div class="content-p44">' +
          '<div id="list-block-p44">' +
            '<div id="selector-p44">' +
              '<div id="triangle-left-p44" class="selector-p44-item nav-p44"></div>' +
              '<input id="p44-input" class="controls" type="text" placeholder="Enter a location" class="input-location-p44">' +
              '<div id="triangle-right-p44" class="selector-p44-item nav-p44"></div>' +
            '</div>'+
            '<ul id="p44-suggestions"></ul>' +
            '<div id = "tabs-p44">' +
              '<div id="new-button-p44" class="button-p44 button-common-p44 active-p44">New Place</div>' +
              '<div id="fit-map-p44" class="button-p44 button-common-p44 active-p44">Show All</div>' +
            '</div>' +
          '</div>' +
          '<div id="show-hide-p44"><div class="triangle-p44 right"></div></div>' +
          '<div id="map-p44"></div>' +
        '</div>' +
        '<div class="foot-p44">' +
          '<span class="right">' +
            '<span>powered by </span>' +
            '<a href="http://vertalab.com?rokect-map" target="_blank">VertaLab</a>' +
          '</span>' +
          '<span class="left">' +
            '<span>Like it? </span>' +
            '<a href="https://chrome.google.com/webstore/detail/rocket-map/flnpifehdjkdpinkkcbjidfcallfbkim/reviews" target="_blank">Send us a message</a>' +
          '</span>' +
        '</div>' +
      '</div>';
      body.appendChild(popup);
      document.getElementsByClassName('close-p44')[0].addEventListener('click', function() {
        popup.style.display = 'none';
      });
      var navButtons = document.getElementsByClassName('nav-p44');
      Array.prototype.slice.call(navButtons,0).forEach(function(navEl) {
        navEl.addEventListener('click', function(evt) {
          window.P44.setCurrentPlaceEvt(evt);
        });
      });
      document.getElementById('new-button-p44').addEventListener('click', function() {
        window.P44.newPlace();
      });
      document.getElementById('fit-map-p44').addEventListener('click', function() {
        window.P44.fitMarkers();
      });
      this.input = document.getElementById('p44-input');
      document.getElementById('p44-input').addEventListener('keyup', function(evt) {
        window.P44.findSuggestions(evt);
      });
      this.subscribeToMoveEvent();
    },

    subscribeToMoveEvent: function(){
      var coordinates = {boxoffsetx:0, boxoffsety:0, started: false, width: 0, height: 0, resized: false};
      var self = this;
      document.getElementsByClassName("head-p44")[0].onmousedown = function(e){
        var offset = document.getElementsByClassName("head-p44")[0].getBoundingClientRect();
        coordinates.boxoffsetx = (e.pageX - offset.left - 5);
        coordinates.boxoffsety = e.pageY-offset.top - 5;
        coordinates.started = true;
        document.getElementsByClassName("overlay-p44")[0].className += ' dragging';
      };
      document.getElementsByClassName("popup-p44")[0].onmousedown = function(e){
        coordinates.width = document.getElementsByClassName("popup-p44")[0].offsetWidth - 10;
        coordinates.height = document.getElementsByClassName("popup-p44")[0].offsetHeight - 10;
      };
      document.onmousemove = function(e){
        if(e.which === 1){
          if(coordinates.started){
            var el = document.getElementsByClassName("popup-p44")[0];
            el.style.left = e.pageX - coordinates.boxoffsetx + "px";
            el.style.top = e.pageY- document.body.scrollTop - 15 + "px";
          }
          var width =  document.getElementsByClassName("popup-p44")[0].offsetWidth - 10;
          var height =  document.getElementsByClassName("popup-p44")[0].offsetHeight - 10;
          if(((width!==coordinates.width)||(height!==coordinates.height))&&(Math.abs(width - coordinates.width)< 50)){
            coordinates.resized = true;
            var map = document.getElementById("map-p44");
            map.style.width = map.offsetWidth + width - coordinates.width + "px";
            map.style.height = map.offsetHeight + height - coordinates.height + "px";
            document.getElementsByClassName("foot-p44")[0].style.width = map.offsetWidth + width - coordinates.width + "px";
            document.getElementsByClassName("content-p44")[0].style.height = map.offsetHeight + height - coordinates.height + "px";
            coordinates.width =  width;
            coordinates.height =  height;
          }
        }
      };
      document.onmouseup = function(e) {
        if (coordinates.started) {
          coordinates.started = false;
          document.getElementsByClassName("overlay-p44")[0].className = document.getElementsByClassName("overlay-p44")[0].className.replace(/\b dragging\b/,'');
        }
        if (coordinates.resized) {
          coordinates.resized = false;
          coordinates.width = document.getElementsByClassName("popup-p44")[0].offsetWidth - 10;
          coordinates.height = document.getElementsByClassName("popup-p44")[0].offsetHeight - 10;
          google.maps.event.trigger(self.getMap(), 'resize');
        }
      };
      document.getElementById("show-hide-p44").onclick = function(){
        var triangle = document.getElementsByClassName("triangle-p44")[0];
        var popup = document.getElementsByClassName("popup-p44")[0];
        if (triangle.className.match(/\bright\b/)){
          document.getElementById("list-block-p44").style.display = "none";
          coordinates.width = popup.offsetWidth - 10 - 240;
          coordinates.height = popup.offsetHeight - 10;
          popup.style.width = coordinates.width + "px";
          popup.style.minWidth = "420px";
          if (popup.style.left !== "") popup.style.left = document.getElementsByClassName("head-p44")[0].getBoundingClientRect().left + 240 - 5 + "px";
          triangle.className = triangle.className.replace(/\b right\b/,'');
          triangle.className += ' left';
        } else {
          document.getElementById("list-block-p44").style.display = "block";
          coordinates.width = document.getElementsByClassName("popup-p44")[0].offsetWidth - 10 + 240;
          coordinates.height = document.getElementsByClassName("popup-p44")[0].offsetHeight - 10;
          popup.style.width = coordinates.width + "px";
          popup.style.minWidth = "660px";
          if (popup.style.left !== "") popup.style.left = document.getElementsByClassName("head-p44")[0].getBoundingClientRect().left - 240 - 5 + "px";
          triangle.className = triangle.className.replace(/\b left\b/,'');
          triangle.className += ' right';
        }
      };
    },
    // init functions end
  };
};
window.initP44Warning = function(){
  var body = document.getElementsByTagName('body')[0];
  var popup = document.createElement('div');
  this.popup = popup;
  popup.className = 'overlay-p44';
  popup.id = 'popup-p44';
  popup.innerHTML = '<div class="popup-p44" style="width: 400px">' +
    '<div class="head-p44">'+
      '<div class="close-p44">x</div>' +
    '</div>'+
    '<div class="content-p44">' +
      '<div id="map-p44"><h4 style="font-size: 22px; margin-top: 160px; text-align: center;">We are sorry, Rocket Map cannot work on this page.</h4></div>' +
    '</div>' +
    '<div class="foot-p44">' +
      '<span class="right">' +
        '<span>powered by </span>' +
        '<a href="http://vertalab.com?rokect-map" target="_blank">VertaLab</a>' +
      '</span>' +
      '<span class="left">' +
        '<span>Like it? </span>' +
        '<a href="https://chrome.google.com/webstore/detail/rocket-map/flnpifehdjkdpinkkcbjidfcallfbkim/reviews" target="_blank">Send us a message</a>' +
      '</span>' +
    '</div>' +
  '</div>';
  body.appendChild(popup);
  document.getElementsByClassName('close-p44')[0].addEventListener('click', function() {
    popup.style.display = 'none';
  });

  var coordinates = {boxoffsetx:0, boxoffsety:0, started: false, width: 0, height: 0, resized: false};

  var self = this;
  document.getElementsByClassName("head-p44")[0].onmousedown = function(e){
    var offset = document.getElementsByClassName("head-p44")[0].getBoundingClientRect();
    coordinates.boxoffsetx = (e.pageX - offset.left - 5);
    coordinates.boxoffsety = e.pageY-offset.top - 5;
    coordinates.started = true;
    document.getElementsByClassName("overlay-p44")[0].className += ' dragging';
  };
  document.getElementsByClassName("popup-p44")[0].onmousedown = function(e){
    coordinates.width = document.getElementsByClassName("popup-p44")[0].offsetWidth - 10;
    coordinates.height = document.getElementsByClassName("popup-p44")[0].offsetHeight - 10;
  };
  document.onmousemove = function(e){
    if(e.which === 1){
      if(coordinates.started){
        var el = document.getElementsByClassName("popup-p44")[0];
        el.style.left = e.pageX - coordinates.boxoffsetx + "px";
        el.style.top = e.pageY- document.body.scrollTop - 15 + "px";
      }
      var width =  document.getElementsByClassName("popup-p44")[0].offsetWidth - 10;
      var height =  document.getElementsByClassName("popup-p44")[0].offsetHeight - 10;
      if((width!==coordinates.width)||(height!==coordinates.height)){
        coordinates.resized = true;
        var map = document.getElementById("map-p44");
        map.style.width = map.offsetWidth + width - coordinates.width + "px";
        map.style.height = map.offsetHeight + height - coordinates.height + "px";
        document.getElementsByClassName("foot-p44")[0].style.width = map.offsetWidth + width - coordinates.width + "px";
        document.getElementsByClassName("content-p44")[0].style.height = map.offsetHeight + height - coordinates.height + "px";
        coordinates.width =  width;
        coordinates.height =  height;
      }
    }
  };
  document.onmouseup = function(e) {
    if (coordinates.started) {
      coordinates.started = false;
      document.getElementsByClassName("overlay-p44")[0].className = document.getElementsByClassName("overlay-p44")[0].className.replace(/\b dragging\b/,'');
    }
    if (coordinates.resized) {
      coordinates.resized = false;
      coordinates.width = document.getElementsByClassName("popup-p44")[0].offsetWidth - 10;
      coordinates.height = document.getElementsByClassName("popup-p44")[0].offsetHeight - 10;
      google.maps.event.trigger(self.getMap(), 'resize');
    }
  };
}

window.P44Warning = function(){
  if(!document.getElementById("popup-p44")) window.initP44Warning();
  else document.getElementById("popup-p44").style.display = "block";
}

var evt = document.createEvent('Event');
evt.initEvent('P44appLoaded', true, false);

// fire the event
document.dispatchEvent(evt);
