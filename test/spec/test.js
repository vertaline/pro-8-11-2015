/* global describe, it */

(function () {
  'use strict';

  describe('P44 es6 crash test', function () {
    describe('check out if class is valid', function () {
      it('should be an object', function () {
        var evt = document.createEvent('Event');
        evt.initEvent('P44appLoaded', true, false);
        // fire the event
        setTimeout(function () {
          global.class = window.P44;
          expect(window.P44).to.be.an('object');
        }, 30000);
      });
    });
  });

  describe('Popup injecting test', function () {
    describe('check out if P44 injects popup into document', function () {
      it('should exist #popup-p44 element in the document', function () {
          setTimeout(function () {
        global.class.initContainer();
        expect($('#popup-p44').length).to.be.equal(1);
                }, 30000);
      });
    });
  });


})();
